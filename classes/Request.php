<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 19:38
 */

class Request
{
    public static function is($uri = null)
    {
        if ($uri && self::clear($_SERVER['REQUEST_URI']) == self::clear($uri)) {
            return true;
        }
        return false;
    }

    private static function clear($uri) {
        return preg_replace(['/^\//', '/\/$/', '/\..*/'], '', $uri);
    }

    public static function exists($type = 'post') {
        switch ($type) {
            case 'post':
                return (!empty($_POST)) ? true : false;
                break;
            case 'get':
                return (!empty($_GET)) ? true : false;
                break;
            default:
                return false;
                break;
        }
    }

    public static function get($item) {
        if (isset($_POST[$item])) {
            return $_POST[$item];
        } else if (isset($_GET[$item])) {
            return $_GET[$item];
        }
        return '';
    }
}