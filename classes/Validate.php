<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 07.12.2018
 * Time: 14:51
 */

class Validate
{
    private $_db = null;
    private $_passed = false;
    private $_errors = [];

    public function __construct()
    {
        $this->_db = DB::getPDO();
    }

    public function check($source, $items = [])
    {
        foreach ($items as $item => $rules) {
            foreach ($rules as $rule => $rule_value) {
                $value 	= trim($source[$item]);
                $item 	= escape($item);

                if ($rule === 'required' && empty($value)) {
                    $this->addError("{$item} is required");
                } else if (!empty($value)) {
                    switch ($rule) {
                        case 'min':
                            if (strlen($value) < $rule_value) {
                                $this->addError("{item} must be a minimum of {$rule_value} characters");
                            }
                            break;
                        case 'max':
                            if (strlen($value) > $rule_value) {
                                $this->addError("{item} must be no longer than {$rule_value} characters");
                            }
                            break;
                        case 'matches':
                            if ($value != $source[$rule_value]) {
                                $this->addError("{$rule_value} must match {$item}");
                            }
                            break;
                        case 'unique':
                            $sql = "SELECT * FROM $rule_value WHERE $item =:value";
                            if ($query = $this->_db->prepare($sql)) {
                                $query->bindParam(':value',$value);
                                $query->execute();
                            }
                            if ($query->rowCount()) {
                                $this->addError("{$item} already exists");
                            }
                            break;
                    }
                }
            }
        }

        if (empty($this->_errors)) {
            $this->_passed = true;
        }

        return $this;
    }

    private function addError($error)
    {
        $this->_errors[] = $error;
    }

    public function errors()
    {
        return $this->_errors;
    }

    public function passed()
    {
        return $this->_passed;
    }
}