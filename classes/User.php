<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 18:54
 */

class User
{
    private $_db;
    private $_tableName = 'users';
    private $_allowed = ['email', 'password', 'salt', 'firstname', 'lastname', 'gender', 'birthday', 'admin'];
    private $_data;
    private $_isAuthenticated;
    private $_sessionName;

    public function __construct($user = null)
    {
        $this->_db = DB::getPDO();
        $this->_sessionName = config('session/session_name');

        if (!$user) {
            if (Session::exists($this->_sessionName)) {
                $user = Session::get($this->_sessionName);

                if ($this->find($user)) {
                    $this->_isAuthenticated = true;
                } else {
                    self::logout();
                }
            }
        } else {
            $this->find($user);
        }
    }

    /**
     * Finds User by ID or E-mail.
     */
    public function find($user = null)
    {
        if ($user) {
            $field = (is_numeric($user)) ? 'id' : 'email';

            $sql = "SELECT * FROM $this->_tableName WHERE {$field} =:user";
            $query = $this->_db->prepare($sql);
            $query->bindParam(":user", $user);
            $query->execute();
            $row = $query->rowCount();
            $results = $query->fetchAll( PDO::FETCH_OBJ );

            $this->_data = $results[0];

            return ($row > 0) ? $results : false;
        }

        return false;
    }

    /**
     * Authentication User. Check email and password pair matches.
     */
    public function auth($email = null, $password = null)
    {
        $user = $this->find($email);

        if ($user) {
            if ($this->data()->password === Hash::make($password . $this->data()->salt)) {
                Session::put($this->_sessionName, $this->data()->id);

                return true;
            }
        }

        return false;
    }

    /**
     * Logout.
     */
    public function logout()
    {
        Session::delete($this->_sessionName);
        Redirect::to('/login.php');
    }

    /**
     * Get User data.
     */
    public function data()
    {
        return $this->_data;
    }

    /**
     * Check User authentication.
     */
    public function isAuth()
    {
        return $this->_isAuthenticated;
    }

    /**
     * Creates a new User.
     */
    public function create($data = [])
    {
        $allowed = $this->_allowed;
        $table = $this->_tableName;

        $sql = "INSERT INTO $table SET " . pdoSet($allowed,$values, $data);

        if ($query = $this->_db->prepare($sql)) {
            return ($query->execute($values)) ? true : false;
        }
        return false;
    }

    /**
     * Deletes User by UD.
     */
    public function delete($id = null)
    {
        $table = $this->_tableName;
        $id = $this->data()->id;

        $sql = "DELETE FROM $table WHERE id=:id";

        if ($query = $this->_db->prepare($sql)) {
            $query->bindParam(':id',$id,PDO::PARAM_INT);
            return ($query->execute()) ? true : false;
        }
        return false;
    }

    /**
     * Update User data.
     */
    public function update($data = [])
    {
        $allowed = $this->_allowed;
        $table = $this->_tableName;

        $sql = "UPDATE $table SET " . pdoSet($allowed,$values, $data) . " WHERE id = :id";

        if ($query = $this->_db->prepare($sql)) {
            $values['id'] = $data['id'];
            return ($query->execute($values)) ? true : false;
        }
        return false;
    }

    /**
     * Checks for admin rights.
     */
    public function isAdmin()
    {
        return $this->data()->admin ? true : false;
    }

    /**
     * Returns full name.
     */
    public function name()
    {
        return ($this->data()->firstname . ' ' . $this->data()->lastname);
    }

    public function exists()
    {
        return (!empty($this->_data)) ? true : false;
    }
}