<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 18:35
 */

class DB
{
    private $_pdo;

    public function __construct()
    {
        try {
            $this->_pdo = new PDO(
                'mysql:host=' . config('mysql/host') . ';dbname=' . config('mysql/db'),
                config('mysql/username'),
                config('mysql/password')
            );
        }
        catch(PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * Gets PDO to database connection.
     */
    public static function getPDO()
    {
        if (self::class !== PDOException::class) {
            $db = new DB;
            return $db->_pdo;
        }
    }
}