<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 07.12.2018
 * Time: 13:32
 */

class DataView
{
    private $_db;
    private $_name;
    private $_count = 0;

    public function __construct($name = 'users')
    {
        $this->_db = DB::getPDO();
        $this->_name = $name;
    }

    /**
     * Prepares select-query with sorting condition.
     */
    public function select($sort_by = 'id', $dir = 'asc')
    {
        $db = $this->_db;

        $sort_by = backticks($sort_by);

        // allowed sort directions
        $dirs = ["asc","desc"];
        $key  = array_search(strtolower($dir), $dirs);
        $dir  = $dirs[$key];

        $sql = "SELECT * FROM $this->_name ORDER BY $sort_by $dir";
        $query = $db->prepare($sql);

        return $query->queryString;
    }

    /**
     * Counts number of rows in table.
     */
    public function count()
    {
        $n_rows = $this->_db->query("select count(*) from $this->_name")->fetchColumn();
        return $this->_count = $n_rows;
    }

    /**
     * Counts number of columns in table.
     */
    public function columns()
    {
        try {
            $db = $this->_db;
            $sql = "SHOW COLUMNS FROM {$this->_name}";
            $query = $db->prepare($sql);
            $query->execute();
            $columns = $query->fetchAll( PDO::FETCH_OBJ );

            $result=array();
            foreach ($columns as $column) {
                $result[] = $column->Field;
            }

            return $result;
        } catch (Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Checks for empty.
     */
    public function isEmpty()
    {
        return $this->count() == 0;
    }

    /**
     * Draws table for data view.
     */
    public function drawTable($query)
    {
        $stmt = $this->_db->prepare($query);
        $stmt->execute();

        if($stmt->rowCount()>0) {
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                ?>

                <tr data-id="<?= $row['id'] ?>">
                    <? foreach ($row as $field => $value) : ?>
                        <td class="value"><?= escape($value)?></td>
                    <? endforeach; ?>

                    <td class="text-center">
                        <a href="#editUserModal" class="mx-3 btn-link text-secondary show" data-toggle="modal"><i class="fa fa-eye" data-toggle="tooltip" title="Show"></i></<a>
                        <a href="#editUserModal" class="mx-3 btn-link text-secondary edit" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                        <a href="#deleteUserModal" class="mx-3 btn-link text-secondary delete" data-toggle="modal"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                    </td>
                </tr>

                <?
            }
        } else {
            ?>
            <tr>
                <td>Nothing here...</td>
            </tr>
            <?php
        }
    }

    /**
     * Sets limit of rows and offset of page in query.
     */
    public function paging($query, $per_page)
    {
        $starting_position = 0;
        if (isset($_GET["page"])) {
            $starting_position = ($_GET["page"] - 1) * $per_page;
        }
        $new_query = $query." limit $starting_position, $per_page";
        return $new_query;
    }

    /**
     * Draws pagination.
     */
    public function pagination($query, $per_page)
    {
        $self = $_SERVER['PHP_SELF'];

        $stmt = $this->_db->prepare($query);
        $stmt->execute();

        $total_no_of_records = $stmt->rowCount();

        if($total_no_of_records > 0) {
            $total_no_of_pages = ceil($total_no_of_records / $per_page);
            $current_page = 1;
            if (isset($_GET["page"])) {
                $current_page = $_GET["page"];
            }
            $previous = $current_page - 1;
            $next = $current_page + 1;
            ?>

            <ul class="pagination justify-content-center">

                 <? if ($current_page != 1) : ?>
                    <li class="page-item"><a class="page-link" href="<?= $self ?>?page=1">First</a></li>
                    <li class="page-item"><a class="page-link" href="<?= $self ?>?page=<?= $previous ?>">&larr;</a></li>
                 <? endif; ?>

                <? for ($i=1; $i <= $total_no_of_pages; $i++) : ?>
                    <? if ($i == $current_page) : ?>
                        <li class="page-item active text-dark"><a class="page-link" href="<?= $self ?>?page=<?= $i ?>"><?= $i ?></a></li>
                    <? else : ?>
                        <li class="page-item"><a class="page-link" href="<?= $self ?>?page=<?= $i ?>"><?= $i ?></a></li>
                    <? endif; ?>
                <? endfor; ?>

                <? if ($current_page != $total_no_of_pages) : ?>
                    <li class="page-item"><a class="page-link" href="<?= $self ?>?page=<?= $next ?>">&rarr;</a></li>
                    <li class="page-item"><a class="page-link" href="<?= $self ?>?page=<?= $total_no_of_pages ?>">Last</a></li>
                <? endif; ?>
            </ul>

            <?php
        }
    }
}
?>