-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 08 2018 г., 08:04
-- Версия сервера: 5.7.20
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sibers`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` varbinary(32) NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `salt`, `birthday`, `gender`, `admin`) VALUES
(1, 'Admin', 'Admin', 'admin@mail.com', 'fa32ae5c203c57509154a6cff17b1fda5a9072c60add52e7725bf5ced3bfc5ae', 0x984119c94b081489c37fedc896f36c7edac5c31883da51dafdc6d84a41f8c014, '1996-12-01', 'male', 1),
(2, 'User', 'User', 'user@mail.com', 'ea2f981cd6cffef8fe803ae1df8e90655f3dfeec4ea94b03f1110a3fd6a15a7d', 0x6a95af49f18ca66ff80cb07567bacc40553fb2613b8b0243678c36954d55b96e, '2010-12-01', 'male', 0),
(3, 'Oleg', 'Yaz', 'oleg@ya.ru', '466ef0ec8e17846ba8bb894451e2c7fe882060198e180f6ea6cc33c09bcbf01d', 0x19a0a0444f51b7dd698749855dea0df8b1081023d742d7de9300c00ab1043536, '2018-12-07', 'male', 1),
(11, 'Yalta', 'Parus', 'HI@mail.ru', 'e265bfcdb0d66282cfd344f7736fd838273b9bad0e33a313b6b24255dd1cd4ee', 0x28e4856782ba699d0f18f88e38ea2595d60e8a43c5c5fc3520ec72eb1a3bf4c9, '2018-12-01', 'male', 0),
(25, 'qweqwe', 'qweqwe', 'qweqwe@mail.ru', 'c49458459fc23c9477405521363f42a42c7641ab8b3d2489fe246c4ec4146337', 0x2c87f53d8e93d9507977d54b7f9c225426627e99a2aed7b52cee39086645f008, '1111-11-11', 'male', 1),
(26, 'admin', '22222', 'LGtLu@mail.ru', '7c9a8f7380878598250b737d5ffe9702871b358524bad667132fe96db0eece83', 0x33e7bd22832b436b0dfd60ac3a561bd335744ec717479c18860cc8adc01ebae5, '0001-11-11', 'female', 1),
(33, 'Bjpfvxmuee', 'Jpzfriibxz', 'pppp@mail.ru', '9cf93c97ae299faf2e9f02658a62ef816a9d0c10fe269baa830d685dcf1289fa', 0x38845318870264cb22c823961e3aabb1aec7e05429bcef142a4131e1dfb090ae, '0311-03-22', 'male', 1),
(34, 'Qdyetbyvgs', 'Gvzdcbaesj', 'fJpQx@mail.ru', '53e57ba3261a024795bd85824a70b9fb77502877112f5697ea0470b8a745a3e4', 0x8f7ac0954d3b437ac529f8a94684478fbfbb1f34f45853ba933606db20a4369b, '0123-03-12', 'male', 0),
(35, 'eee', 'Yqcevkqlvt', 'zeeoI@mail.ru', '8537bba71c6169d1095c76b280bb609d8e433577bc3a53809b47ac6381f34d4e', 0x3ebeeeafbe94c07a9c1277593d07612bcc8b0fd2c40050ed383d8bc08470a5f7, '2018-09-03', 'male', 0),
(36, 'Jrozcylacu', 'Pcxfornvuu', 'mFeEM@mail.ru', '7b486ded9b1cba6e9794ba49c79aa45312d290cbb1afa404d9410a2204747d55', 0xf2849a03c2258b0e28147fa8b59847ef5d7d60e5cea7c08b92ef62cc9212abea, '0111-11-11', 'female', 0),
(37, 'Rpswqlzknw', 'Oxxfeuwrmk', 'pczqT@mail.ru', '3cde81e448c2d11d0d024106a1eac97ab002732534a4a6d48f9f7c80a4646909', 0xfb07eb3d5648454fe2916e075a38819e2c2e7344382835e24c67333ac220195a, '0222-02-22', 'female', 0),
(38, 'New', 'User', 'vga@m.ru', '12cfc6d4552cd6cbe6cfa08cbd58399846b9c6afb60b86c21bbf2144c7b8e1e9', 0xa57b6f0ae159c0ad9e71e16188ac746214ce837d78e601b6f7810ee23258c55a, '3333-03-12', 'male', 1),
(40, 'Toyyqeyzxb', 'Qlvyqtdonx', 'KDTmN@mail.ru', '1e2b1a44f6fa922595991e6c1b2e86382f95d495944c3c026dc6cc5ad68d19fb', 0xae43e32b40ece9f32407d6dacf0f56366d0516aafd1bbef147e02d21ad934653, '0222-02-22', 'male', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
