<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 07.12.2018
 * Time: 21:13
 */
?>

<? if (Session::exists('error')) :?>
    <div class="alert alert-danger" role="alert">
        <i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp;
        <span><?= Session::flash( 'error' )?></span>
    </div>
<? endif; ?>

<? if (Session::exists('success')) :?>
    <div class="alert alert-success" role="alert">
        <i class="fa fa-exclamation-check" aria-hidden="true"></i>&nbsp;
        <span><?= Session::flash( 'success' )?></span>
    </div>
<? endif; ?>