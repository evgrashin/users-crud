<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 17:35
 */
?>

<nav class="navbar navbar-expand-md navbar-dark bg-dark">

    <a class="navbar-brand text-uppercase" href="/">
        <?= $_SERVER['HTTP_HOST']; ?>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left side Of navbar -->
        <ul class="navbar-nav mr-auto">
            <li>
            <? if (!empty($user) && $user->isAuth()) : ?>
                <a href="#" class="<? if (Request::is('profile')) echo "active"; ?> nav-link"><?= $user->name(); ?></a>
            <? endif ?>
            </li>
        </ul>

        <!-- Right side of navbar -->
        <ul class="navbar-nav ml-auto">
            <? if (!empty($user) && $user->isAuth()) : ?>

            <li>
                <form id="logout-form" action="/controllers/logout.php" method="POST" class="hidden">
                </form>
                <a id="logout" class="nav-link" href="#" onclick="$('#logout-form').submit();">Logout</a>
            </li>

            <? else : ?>

            <li><a class="<? if (Request::is('login')) echo "active"; ?> nav-link" href="/login.php">Login</a></li>
            <li><a class="<? if (Request::is('register')) echo "active"; ?> nav-link" href="/register.php">Register</a></li>

            <? endif ?>
        </ul>
    </div>
</nav>