<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 16:59
 */

require_once 'core/init.php';

$page->title = "Login";

$user = new User();
if ($user->isAuth()) {
    Session::flash('success', 'Уou are already logged in to your account.');
    Redirect::to('/index.php');
}

require_once "layout/header.php";
?>

<div class="row">
    <form method="post" action="/controllers/login.php" class="mx-auto col-lg-4 col-md-6 col-sm-6">
        <div class="card card-default text-center mt-5">
            <div class="card-header">
                <h1><?= $page->title ?></h1>
            </div>
            <div class="card-body">
                <!-- Alerts -->
                <? require_once "layout/_particles/alerts.php"; ?>

                <!-- Input Email -->
                <div class="input-group input-group-lg mb-3">
                    <div class="input-group-prepend input-group-lg"><i class="input-group-text fa fa-at" aria-hidden="true"></i></div>
                    <input type="email" class="form-control" name="email" placeholder="E-mail" minlength="6" maxlength="255" required >
                </div>

                <!-- Input Password -->
                <div class="input-group input-group-lg mb-3">
                    <div class="input-group-prepend input-group-lg"><i class="input-group-text fa fa-lock" aria-hidden="true"></i></div>
                    <input type="password" class="form-control" name="password" placeholder="Password" minlength="6" maxlength="255" required >
                </div>

                <!-- Button Submit -->
                <button type="submit" class="btn btn-lg btn-primary">Sign In</button>
                <a href="/register.php" class="btn btn-lg btn-light">Register</a>
            </div>
        </div>
    </form>
</div>

<? require_once "layout/footer.php"; ?>