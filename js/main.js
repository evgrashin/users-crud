$(document).ready(function(){
    // Activate tooltip.
    $('[data-toggle="tooltip"]').tooltip();

    // Load user info to modal form and show it.
    $("a.show").on('click', function() {
        let id = $(this).closest('tr').data('id');

        $.ajax({
            method: 'post',
            url: "/controllers/users.php",
            data: {
                'act': 'show',
                'id': id,
            },
            dataType: 'json',
            success: function (data) {
                console.log('success', arguments);

                // Fill inputs and selectors
                $.each(data, function (k, v) {
                    $("#editUserModal [name='" + k + "']").val(replaceQuotes(v));
                    $("#editUserModal [name='" + k + "']").attr('disabled', true);
                });

                $("#editUserModal [name='password']").val('').parent('div').addClass('d-none');
                $("#editUserModal [name='password_again']").val('').parent('div').addClass('d-none');
                $("#editUserModal .modal-footer").addClass('d-none');
            },
            error: function () {
                console.log('error', arguments);
            },
            complete: function () {
                console.log('complete', arguments);
            }
        }).done(function () {
            console.log('done', arguments);
        });
    });

    // Load user info to modal form for editing.
    $("a.edit").on('click', function() {
        let id = $(this).closest('tr').data('id');

        $.ajax({
            method: 'post',
            url: "/controllers/users.php",
            data: {
                'act': 'show',
                'id': id,
            },
            dataType: 'json',
            success: function (data) {
                console.log('success', arguments);

                $.each(data, function (k, v) {
                    $("#editUserModal [name='" + k + "']").val(replaceQuotes(v));
                    $("#editUserModal [name='" + k + "']").attr('disabled', false);
                });

                $("#editUserModal [name='password']").val('').parent('div').removeClass('d-none');
                $("#editUserModal [name='password_again']").val('').parent('div').removeClass('d-none');
                $("#editUserModal .modal-footer").removeClass('d-none');
            },
            error: function () {
                console.log('error', arguments);
            },
            complete: function () {
                console.log('complete', arguments);
            }
        }).done(function () {
            console.log('done', arguments);
        });
    });

    // Update user info.
    $("#editUserModal form").submit(function(e) {
        var url = $(this).attr("action");
        var formData = {};
        $(this).find("input[name],select[name]").each(function (index, node) {
            formData[node.name] = node.value;
        });
        e.preventDefault();

        formData.act = 'upd';

        console.log(formData);

        $.post(url, formData).done(function (data) {
            sortTable();
        });
    });

    // Delete user.
    $("a.delete").on('click', function(e) {
        if (!confirm('Are you sure?')) {
            return;
        }

        let $tr = $(this).closest('tr');
        let id = $tr.data('id');

        $.ajax({
            method: 'post',
            url: "/controllers/users.php",
            data: {
                'act': 'del',
                'id': id,
            },
            dataType: 'json',
            complete: function (data) {
                window.location.reload();
            },
        });
    });

    // Sort table by chosen field.
    // Set row limit per page.
    $(".table-title select").on('change', function() {
        sortTable(this);
    });

    // Set GET parameters for sort.
    function sortTable(e) {
        let sort_by = $("#sort_by").val();
        let dir = $("#dir").val();
        let limit = $("#limit").val();

        const params = new URLSearchParams(window.location.search);

        params.get('page');
        params.set('sort_by', sort_by);
        params.set('dir', dir);
        params.set('limit', limit);

        window.history.replaceState({}, '', `${location.pathname}?${params}`);
        location.reload();
    };

    // Add current page to url search parameters
    // without erasing others.
    $("a.page-link").click(function(e) {
        e.preventDefault();
        const params = new URLSearchParams(window.location.search);
        params.set('page', getParameterByName('page', $(this).attr('href')));

        window.history.replaceState({}, '', `${location.pathname}?${params}`);
        location.reload();
    })

    // Remove quotes in JSON.
    function replaceQuotes (string) {
        return string.toString().replace(/\"/g, "").replace();
    }

    // Get search parameter from url by name.
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }
});