<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 16:52
 */

require_once 'core/init.php';

$user = new User();
if (!$user->isAuth()) {
    Redirect::to('login.php');
} elseif (!$user->isAdmin()) {
    $user->logout();
};

$page->title = "Main page";

require_once "layout/header.php";

$data_view = new DataView('users');

if (!empty($_GET) && isset($_GET['sort_by']) && isset($_GET['dir']) && isset($_GET['limit'])) {
    $select_query = $data_view->select($_GET['sort_by'], $_GET['dir'], $_GET['limit']);
} else {
    $select_query = $data_view->select();
}
?>

<!-- Alerts -->
<div id="alerts">
    <? require_once "layout/_particles/alerts.php"; ?>
</div>


<div class="table-wrapper mx-auto">
    <div class="table-title">
        <h2 class="pull-left"><b>Users</b></h2>

        <div class="pull-right">

            <!-- Sort field -->
            <label for="sort_by">&nbsp;Sort by&nbsp;</label>
            <select class="form-control-sm" name="sort_by" id="sort_by">
                <? foreach ($data_view->columns() as $column) : ?>
                    <option
                        value="<?= escape($column)?>"
                        <? if (isset($_GET["sort_by"]) && $_GET["sort_by"] == $column) : ?>
                            selected
                        <? endif; ?>
                    >
                        <?= escape($column)?>
                    </option>
                <? endforeach; ?>
            </select>

            <!-- Sort direction -->
            <label for="dir">&nbsp;Direction&nbsp;</label>
            <select class="form-control-sm" name="dir" id="dir">
                <option value="asc"
                    <? if (isset($_GET["dir"]) && $_GET["dir"] == "asc") : ?> selected <? endif; ?>
                >&uarr;</option>
                <option value="desc"
                    <? if (isset($_GET["dir"]) && $_GET["dir"] == "desc") : ?> selected <? endif; ?>
                >&darr;</option>
            </select>

            <!-- Rows limit per page -->
            <label class="" for="limit">&nbsp;Rows&nbsp;</label>
            <select class="form-control-sm" name="limit" id="limit">
                <option value="5"
                    <? if (isset($_GET["limit"]) && $_GET["limit"] == 5) : ?> selected <? endif; ?> >5</option>
                <option value="10"
                    <? if (isset($_GET["limit"]) && $_GET["limit"] == 10) : ?> selected <? endif; ?> >10</option>
                <option value="15"
                    <? if (isset($_GET["limit"]) && $_GET["limit"] == 15) : ?> selected <? endif; ?> >15</option>
            </select>

            <!-- Show & Add & Delete buttons -->
            <a href="/register.php" class="btn btn-success ml-3"> <!--data-toggle="modal"--><i class="fa fa-plus"></i> <span>Add New User</span></a>
        </div>
    </div>

    <? if ($data_view->isEmpty()) : ?>

    <div class="text-center">
        <h1 class="py-5 mx-auto">Table is empty.</h1>

        <a href="#addUserModal" class="btn btn-success btn-lg" data-toggle="modal"><i class="fa fa-plus"></i> <span>Add New User</span></a>
    </div>

    <? else : ?>

    <table id="usersTable" class="table table-striped table-bordered table-hover table-sm table-responsive-md" cellspacing="0">
        <thead>
        <tr>
            <? foreach ($data_view->columns() as $field => $value) : ?>

                <th class="field"><?= escape($value)?></th>

            <? endforeach; ?>

            <th></th>
        </tr>
        </thead>
        <tbody>

        <?
        // Init pagination
        $per_page = 5;
        if (Request::get('limit')) $per_page = Request::get('limit');
        $offset_query = $data_view->paging($select_query, $per_page);

        // Drow table view
        $data_view->drawTable($offset_query);

        // Drow pagination
        $data_view->pagination($select_query, $per_page);
        ?>

        </tbody>
    </table>
    <? endif; ?>
</div>


<!-- Show / Edit modal HTML -->
<div id="editUserModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="/controllers/users.php">
                <div class="modal-header">
                    <h4 class="modal-title">Edit User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">
                    <!-- Alerts -->
                    <? require_once "layout/_particles/alerts.php"; ?>

                    <input type="hidden" name="id" id="id" >

                    <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input class="form-control" type="text" name="firstname" id="firstname" value="" required/>
                    </div>

                    <div class="form-group">
                        <label for="lastname">Last Name</label>
                        <input class="form-control" type="text" name="lastname" id="lastname" value="" required/>
                    </div>

                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input class="form-control" type="email" name="email" id="email" value="" autocomplete="off" required/>
                    </div>

                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password" id="password" required/>
                    </div>

                    <div class="form-group">
                        <label for="password_again">Confirm your password</label>
                        <input class="form-control" type="password" name="password_again" id="password_again" required/>
                    </div>

                    <div class="form-group">
                        <label for="gender">Gender: </label>
                        <select class="form-control" name="gender" id="gender" required>
                            <option selected disabled hidden> — </option>
                            <option value="male">M</option>
                            <option value="female">F</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="birthday">Birth Day</label>
                        <input class="form-control" type="date" name="birthday" id="birthday" required/>
                    </div>

                    <? if (isset($user) && $user->isAdmin()) : ?>
                        <div class="form-group">
                            <label for="admin">Administrator: </label>
                            <select class="form-control" name="admin" id="admin" required>
                                <option selected value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                    <? endif; ?>
                </div>

                <div class="modal-footer">
                    <div class="text-center">
                        <button id="edit-modal-btn-confirm" class="btn btn-primary" type="submit" name="update" value="update">Save Changes</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>


<!-- Delete modal HTML -->
<div id="deleteUserModal_" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="/controllers/users.php">
                <div class="modal-header">
                    <h4 class="modal-title">Delete User <strong name="firstname"></strong> <strong name="lastname"></strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this user?</p>
                    <p class="text-warning"><small>This action cannot be undone.</small></p>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                    <input id="delete-modal-btn-confirm" type="button" class="btn btn-danger" value="Delete">
                </div>
            </form>
        </div>
    </div>
</div>


<script src="js/main.js" defer></script>

<? require_once "layout/footer.php"; ?>
