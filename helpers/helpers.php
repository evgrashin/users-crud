<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 18:14
 */

// Sanitize string.
function escape($string)
{
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

// Get option from config.
function config($path = null)
{
    if ($path) {
        $config = $GLOBALS['config'];
        $path = explode('/', $path);

        foreach ($path as $bit) {
            if (isset($config[$bit])) {
                $config = $config[ $bit ];
            }
        }

        return $config;
    }

    return false;
}

// Prepare values for insert / update queries.
function pdoSet($allowed, &$values, $source = []) {
    $set = '';
    $values = [];
    if (!$source) $source = &$_POST;
    foreach ($allowed as $field) {
        if (isset($source[$field])) {
            $set.="`".str_replace("`","``",$field)."`". "=:$field, ";
            $values[$field] = $source[$field];
        }
    }
    return substr($set, 0, -2);
}

// Escape field name.
function backticks($field) {
    return "`".str_replace("`", "``", $field)."`";
}


function generateRandomString($length = 10) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}