<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 17:55
 */

require_once 'core/init.php';

$user = new User();
$page->title = "New Account";

require_once "layout/header.php";
?>

<div class="container">
    <div class="col-md-6 offset-md-3">
        <h1><?= $page->title?></h1>

        <!-- Alerts -->
        <? require_once "layout/_particles/alerts.php"; ?>

        <form action="/controllers/register.php" method="post">

            <div class="form-group">
                <label for="firstname">First Name</label>
                <input class="form-control" type="text" name="firstname" id="firstname" required/>
            </div>

            <div class="form-group">
                <label for="lastname">Last Name</label>
                <input class="form-control" type="text" name="lastname" id="lastname" required/>
            </div>

            <div class="form-group">
                <label for="email">E-mail</label>
                <input class="form-control" type="email" name="email" id="email" autocomplete="off" required/>
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" id="password" required/>
            </div>

            <div class="form-group">
                <label for="password_again">Confirm your password</label>
                <input class="form-control" type="password" name="password_again" id="password_again" required/>
            </div>

            <div class="form-group">
                <label for="gender">Gender: </label>
                <select class="form-control" name="gender" id="gender" required>
                    <option selected disabled hidden> — </option>
                    <option value="male">M</option>
                    <option value="female">F</option>
                </select>
            </div>

            <div class="form-group">
                <label for="birthday">Birth Day</label>
                <input class="form-control" type="date" name="birthday" id="birthday"  required/>
            </div>

            <? if (isset($user) && $user->isAdmin()) : ?>
            <div class="form-group">
                <label for="admin">Administrator: </label>
                <select class="form-control" name="admin" id="admin" required>
                    <option selected value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </div>
            <? endif; ?>

            <div class="text-center">
                <button class="btn btn-lg btn-primary" type="submit" name="register" value="Register">Create Account</button>

                <a href="/Login.php" class="btn btn-lg btn-light <? if ($user->isAuth()) { echo 'd-none'; }?>">Login</a>
            </div>

        </form>
    </div>
</div>

<? require_once "layout/footer.php"; ?>