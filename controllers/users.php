<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 07.12.2018
 * Time: 10:49
 */

require_once '../core/init.php';

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $action = Request::get('act');
    $id     = Request::get('id');

    $user = new User($id);
    if (!$user->exists()) {
        Session::flash('error','User with ID=' . escape($id) . ' not exists.');
        return;
    }
    if (!Session::exists(config('session/session_name'))) {
        Session::flash('error','You are not authenticated.');
        return;
    }
    $name   = $user->name();

    switch ($action) {
        case 'show':
            {
                $field = $value = [];
                $data = [];
                foreach ($user->data() as $field => $value) {
                    $data[$field] = json_encode($value, JSON_FORCE_OBJECT);
                }

                echo json_encode($data);

                break;
            }

        case 'del':
            {
                if ($user->isAdmin()) {
                    Session::flash('error', 'You can\'t delete this user. «' . escape($name) . '» is an administrator.');
                    break;
                }

                if ($user->delete()) {
                    Session::flash('success','User «' . escape($name) . '» was deleted successfully.');
                    break;
                }

                Session::flash('error','Unknown Error. User «' . escape($name) . '» wasn\'t deleted.');
                break;
            }

        case 'upd':
            {
                $validate = new Validate();
                $validation = $validate->check($_POST, [
                    'email'	=> [
                        'fieldName'	=> 'E-mail',
                        'required' 	=> true,
                        'min'		=> 6,
                    ],
                    'password' => [
                        'fieldName'	=> 'Password',
                        'required' 	=> true,
                        'min'		=> 6
                    ],
                    'password_again' => [
                        'fieldName'	=> 'Password Repeat',
                        'required' 	=> true,
                        'min'		=> 6,
                        'matches'	=> 'password'
                    ],
                ]);

                if ($validation->passed()) {
                    $salt = Hash::salt(32);
                    try {
                        $done = $user->update([
                            'id'        => Request::get('id'),
                            'email' 	=> Request::get('email'),
                            'password' 	=> Hash::make(Request::get('password'),$salt),
                            'salt' 		=> $salt,
                            'firstname' => Request::get('firstname'),
                            'lastname'  => Request::get('lastname'),
                            'gender'    => Request::get('gender'),
                            'birthday'  => Request::get('birthday'),
                            'admin'	    => Request::get('admin') ? 1 : 0,
                        ]);

                        if ($done) {
                            $field = $value = [];
                            $data = [];
                            foreach ($user->data() as $field => $value) {
                                $data[$field] = json_encode($value, JSON_FORCE_OBJECT);
                            }

                            echo json_encode($data);
                            Session::flash('success','User\'s info was updated successfully.');
                        } else {
                            Session::flash('error','Unknown error.');
                        }
                    } catch (Exception $e) {
                        die($e->getMessage());
                    }
                } else {
                    foreach ($validation->errors() as $error) {
                        Session::flash('error', $error);
                        echo $error;
                    }
                }
            }
    }
}