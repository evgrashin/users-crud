<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 19:01
 */

require_once '../core/init.php';

if (Request::exists()) {
    $login = $_POST['email'];
    $password = $_POST['password'];

    $user = new User();

    $isAuthenticated = $user->auth($login, $password);

    if ($isAuthenticated) {
        if ($user->isAdmin()) {
            Redirect::to('/');
        } else {
            Session::flash('error', 'Access deny. Not enough permissions.');
        }
    } else {
        Session::flash('error', 'Incorrect login or password.');
    };

    Redirect::to( '/login.php');
}