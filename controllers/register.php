<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 18:38
 */

require_once '../core/init.php';

if (Request::exists()) {
    $validate = new Validate();
    $validation = $validate->check($_POST, [
        'email'	=> [
            'fieldName'	=> 'E-mail',
            'required' 	=> true,
            'min'		=> 6,
            'unique'	=> 'users'
        ],
        'password' => [
            'fieldName'	=> 'Password',
            'required' 	=> true,
            'min'		=> 6
        ],
        'password_again' => [
            'fieldName'	=> 'Password Repeat',
            'required' 	=> true,
            'min'		=> 6,
            'matches'	=> 'password'
        ],
    ]);

    if ($validation->passed()) {
        $user = new User();
        $salt = Hash::salt(32);
        try {
            $done = $user->create([
                'email' 	=> Request::get('email'),
                'password' 	=> Hash::make(Request::get('password'),$salt),
                'salt' 		=> $salt,
                'firstname' => Request::get('firstname'),
                'lastname'  => Request::get('lastname'),
                'gender'    => Request::get('gender'),
                'birthday'  => Request::get('birthday'),
                'admin'	    => Request::get('admin') ? 1 : 0,
            ]);

            if ($done) {
                Session::flash('success','Account was created successfully.');
            } else {
                Session::flash('error','Unknown error.');
            }
            Redirect::to('/index.php');
        } catch (Exception $e) {
            die($e->getMessage());
        }
    } else {
        foreach ($validation->errors() as $error) {
            Session::flash('error', $error);
        }
        Redirect::to('/register.php');
    }
}