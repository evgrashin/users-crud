<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 18:29
 */

require_once '../core/init.php';

$user = new User();
$user->logout();

Redirect::to( '/' );