<?php
/**
 * Created by PhpStorm.
 * User: Oleg
 * Date: 06.12.2018
 * Time: 17:55
 */

session_start();

/**
 * Main config.
 */
$GLOBALS[ 'config' ] = [
    'mysql' => [
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => '',
        'db' => 'sibers'
    ],
    'session' => [
        'session_name' => 'user'
    ]
];

// The root directory path
define("ROOT_DIR", dirname(dirname(__FILE__)));

spl_autoload_register(function($class) {
    require_once ROOT_DIR . '/classes/' . $class . '.php';
});

require_once ROOT_DIR . '/helpers/helpers.php';

$page = new stdClass();